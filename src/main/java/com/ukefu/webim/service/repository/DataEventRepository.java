package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.DataEvent;

public interface DataEventRepository extends JpaRepository<DataEvent, String>{
	
	public abstract List<DataEvent> findByDataid(String dataid);
	
}
